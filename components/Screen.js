import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    Text,
    TouchableHighlight,
} from 'react-native'
import InputBox from './InputBox'
import VerifyButton from './VerifyButton'
import EnterField from './EnterField'
import TrivialMessage from './TrivialMessage'

 class Screen extends Component {

   forwardFunc = () =>{
        console.log('Hello');
    }

    render() {
        return (
            <View style={styles.container}>
                <TrivialMessage />
                <EnterField />
                <VerifyButton onConfirm={this.forwardFunc}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:5,
    },
})

export default Screen;