import React, { Component } from 'react'

import {
    View,
    Button,
    StyleSheet,
    TextInput,
} from 'react-native'

// import InputBox from './InputBox'


class EnterField extends Component {
    constructor(props){
        super(props)
        this.myRef = React.createRef();
        this.state = {
            boxCount: [{id:1},{id:2},{id:3},{id:4},{id:5},{id:6}],
            value:'',
        };

        const node = this.myRef.current;
        

    }

    handleChange = (text) =>{
        this.setState({
            value:text,
        })
        console.log(this.myRef.current.focus());
    }

    elementBox = (item) => {
        return document.getElementById('item'+item);
    }

    render() {
            return (
                <View style={styles.container}>
                    {this.state.boxCount.map((item,index)=>{
                        return <View style={styles.sub_container} key={index}> 
                                   <View style={styles.container1}>
                                        <View style={styles.input_box}>
                                            <TextInput 
                                                style={styles.entry_field}
                                                onFocus={this.handleFocus}
                                                onChangeText={(text)=>this.handleChange(text)}
                                                defaultValue={this.state.value}
                                                keyboardType="numeric"
                                                ref = {this.myRef}
                                                id={this.state.boxCount.id}
                                                maxLength={1}>
                                            </TextInput>  
                                        </View>
                                </View>
                            </View>
                     })}             
                </View>
            )
        }
}

const styles = StyleSheet.create({
    container:{
        flexDirection:'row',
        flex:1,
   },
   sub_container:{
       flex:1,
       flexDirection:'row',
   },
   input_box:{
    width:'60%',
    
    },
    entry_field:{
    width:'100%',
    textAlign:'center',
    fontSize:25,
    borderBottomWidth:2,
    },
    container1:{
    backgroundColor:'wheat',
    flex:1,
    justifyContent:'center',
    alignItems:'center',
}
})

export default EnterField;
