import React from 'react'

import {
    StyleSheet,
    View,
    Text,
} from 'react-native'

function TitleBar() {
    return (
        <View style={styles.container}>
          <View style={styles.theader_box}>
              <Text style={styles.theader}>Verification Code</Text>
          </View>   
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
      flex:1,
    },
    theader_box:{
      backgroundColor:'lightblue',
      flex:1,
      justifyContent:'center',
      alignItems:'center',
      backgroundColor:'rgb(48, 59, 71)',
    },  
    theader:{
      color:'antiquewhite',
      fontSize: 25,
    }
  });

export default TitleBar;
