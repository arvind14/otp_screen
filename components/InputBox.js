import React, { Component } from 'react'
import { 
    TextInput,
    View,
    StyleSheet,
    Button,
 } from 'react-native'



class InputBox extends Component {
    constructor(props){
        super(props)

        this.inputRef = React.createRef();
        this.state = {
            value:'',
        }

    }

    

   
    render() {
        console.log(this.state.value);
        return (
            <View style={styles.container}>
                <View style={styles.input_box}>
                    <TextInput 
                        type="number"
                        style={styles.entry_field}
                        onChangeText={(text)=>this.handleChange(text)}
                        defaultValue={this.state.value}
                        keyboardType="numeric"
                        ref = {this.inputRef}
                        maxLength={1}></TextInput>  
                </View>
                <Button title="Click" onPress={this.onFocus}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    input_box:{
        width:'60%',
        
    },
    entry_field:{
        width:'100%',
        textAlign:'center',
        fontSize:25,
        borderBottomWidth:2,
    },
    container:{
        backgroundColor:'wheat',
        flex:1,
        justifyContent:'center',
        alignItems:'center',
    }
})

export default InputBox;