import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

// import {
//   Header,
//   LearnMoreLinks,
//   Colors,
//   DebugInstructions,
//   ReloadInstructions,
// } from 'react-native/Libraries/NewAppScreen';

import Screen from './components/Screen'
import TitleBar from './components/TitleBar';

const App = () => {
  return (
    <View style={styles.container}>
      <TitleBar />
      <Screen />
    </View>
  );
};

const styles = StyleSheet.create({
  container:{
    flex:1,
  }
})

export default App;
